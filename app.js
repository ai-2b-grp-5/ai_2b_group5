// const express = require("express")
// const app = express('')
// const path = require('path');

// const userRouter = require('./routes/userRoutes')
// const viewRouter = require('./routes/viewRoutes')

// app.use(express.json())
// app.use('/api/v1/users', userRouter)
// app.use('/', viewRouter)

// app.use(express.static(path.join(__dirname, 'views')))

// module.exports = app

const express = require("express")
const app = express()
const path = require('path');
const cookieParser = require('cookie-parser');



// middleware to parse JSON requests
app.use(express.json());
app.use(cookieParser());



const userRouter = require('./routes/userRoutes')
const viewRouter = require('./routes/viewRoutes')
const adminRouter = require('./routes/adminRoutes');


const bookingRoutes = require('./routes/bookingRoutes');



app.use('/api/v1/bookings', bookingRoutes);

// app.use(express.json())


app.use('/api/v1/users', userRouter)
app.use('/api/v1/admins', adminRouter);
// app.use("/api/v1/booking",bookingRouter);

app.use('/', viewRouter)

app.use(express.static(path.join(__dirname, 'views', 'landingpage')))

module.exports = app
