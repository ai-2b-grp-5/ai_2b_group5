const Admin = require('./../models/adminModels')

exports.getAllAdmins = async (req, res) => {
  try {
    const admins = await Admin.find()
    res.status(200).json({ data: admins, status: 'success' })
  } catch (err) {
    res.status(500).json({ error: err.message })
  }
}

exports.createAdmin = async (req, res) => {
  try {
    const admin = await Admin.create(req.body)
    console.log(req.body.email)
    res.json({ data: admin, status: 'success' })
  } catch (err) {
    res.status(500).json({ error: err.message })
  }
}

exports.getAdmin = async (req, res) => {
  try {
    const admin = await Admin.findById(req.params.id)
    res.json({ data: admin, status: 'success' })
  } catch (err) {
    res.status(500).json({ error: err.message })
  }
}

exports.updateAdmin = async (req, res) => {
  try {
    const admin = await Admin.findByIdAndUpdate(req.params.id, req.body)
    res.json({ data: admin, status: 'success' })
  } catch (err) {
    res.status(500).json({ error: err.message })
  }
}

exports.deleteAdmin = async (req, res) => {
  try {
    const admin = await Admin.findByIdAndDelete(req.params.id)
    res.json({ data: admin, status: 'success' })
  } catch (err) {
    res.status(500).json({ error: err.message })
  }
}
