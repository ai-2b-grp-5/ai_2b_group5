const booking = require('../models/bookingRecordModels')
const jwt = require('jsonwebtoken')

exports.getAllBooking = async (req, res, next) => {
    try {
        const Booking = await booking.find()
        res.status(200).json({data:Booking, status: 'success'})
    }catch (err) {
        res.status(500).json({ error: err.message});
    }
}

exports.createbooking = async(req,res,next)=>{
    const bookings = new booking({...req.body,ownerData:req.user._id})
    try{
        const bookingData = await bookings.save()
        res.json({data:bookingData,status:"success"})

    }catch(err){
        res.status(500).json({error:err.message})
    }
}

exports.getBookingDetailsByIdForUser = async(req,res)=>{
    try{
        console.log("user jsdn,fsmn",req.user)
        const bookingData = await booking.find({ownerData:req.user.id})
        // const bookingDataAndUserData = await bookingData.populate('ownerData')
        res.json({data:bookingData,status:"success"})
        console.log(bookingData)
    }catch(err){
        res.status(500).json({error:err.message})
    }
    
}

exports.getBooking = async (req, res) => {
    try{
        const Booking = await booking.findById(req.params.id);
        res.json({data: Booking, status: "success"});
    }catch (err){
        res.status(500).json({error: err.message});
    }
}

exports.updateBooking = async (req, res) => {
    try{
        const Booking = await booking.findByIdAndUpdate(req.params.id, req.body);
        res.json({data: Booking, status: "success"});
    }catch (err){
        res.status(500).json({error: err.message});
    }
}

exports.deleteBooking = async (req, res) => {
    try{
        const Booking = await booking.findByIdAndDelete(req.params.id);
        res.json({data: Booking, status: "success"});
    }catch (err){
        res.status(500).json({error: err.message});
    }
}

