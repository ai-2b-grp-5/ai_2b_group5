const path = require('path')



/* LOG IN PAGE*/

exports.getLoginForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'landingpage', 'Signin.html'))
}

/* Sign IN PAGE*/
exports.getSignupForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'landingpage','Signup.html'))
}

/* Landingpage PAGE*/
exports.getHome = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'landingpage','landingindex.html'))
    
}

// /* HomePAGE*/
// exports.getIndexHome = (req, res) => {
//     res.sendFile(path.join(__dirname, '../', 'views', 'landingpage','index.html'))
// }



/* HomePAGE*/
    exports.getIndexHome = (req, res) => {

    if (!req.cookies.token) {
      res.redirect('/login'); // modify this line to redirect to login if user is not logged in
    } else {
      res.sendFile(path.join(__dirname, '../', 'views', 'landingpage','index.html'))
    }
  };
  