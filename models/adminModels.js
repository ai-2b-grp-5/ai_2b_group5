const mongoose = require('mongoose')
const validator = require('validator');




const adminSchema = new mongoose.Schema({

    email:{
        type: String,
        require:[true,'Please provide your email'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail,'Please provide a valid email'],
    },
    password:{
        type:String,
        require:[true,'please provide a password!'],
        minlength:8,
        select:false,
    },
  
})

const Admin = mongoose.model('Admin', adminSchema);
module.exports = Admin;
