const { default: mongoose } = require("mongoose")
const User = require("./userModels")
const bookingSchema =new mongoose.Schema({
    carModel:{
        type:String,
        require:[true,"Please provide car model"]
    },
    carColor:{
        type:String,
        require:[true,"Please provide car color"]
    },
    carNumber:{
        type:String,
        require:[true,"Please provide car number"]
    },
    phoneNumber:{
        type:String,
        require:[true,"Please provide car color"]
    },
    startingDate:{
        type:String,
        require:[true,"Please provide starting date "]
    },
    startingTime:{
        type:String,
        require:[true,"please provide starting time"]
    },
    // status:{
    //     type:String,
    //     default:"notAccepted"
        
    // },
    // adminDisplay:{
    //     type:Boolean,
    //     default:true
    // },
    // userDisplay:{
    //     type:Boolean,
    //     default:true
    // },
    ownerData:{
        type:mongoose.Schema.ObjectId,
        required:true,
        ref:"User"
    }

})

const BookingRecord =mongoose.model('BookingRecord',bookingSchema)
module.exports =BookingRecord