const express = require('express')
const adminController = require('./../controllers/adminController')
const authController = require('./../controllers/authController')
const router = express.Router()

// router.post('/signup', authController.signup)
router.post('/login', authController.login)

router 
    .route('/')
    .get(adminController.getAllAdmins)
    .post(adminController.createAdmin)

router
    .route('/:id')
    .get(adminController.getAdmin)
    .patch(adminController.updateAdmin)
    .delete(adminController.deleteAdmin)

module.exports = router
