const express = require('express')
const bookingController = require('./../controllers/bookingController')
const authController = require("./../controllers/authController")

const router = express.Router()

router
    .route('/')
    .get(bookingController.getAllBooking)
    .post(authController.protect,bookingController.createbooking)


router.get('/getBookingDataByIdForUser',
    authController.protect,
    bookingController.getBookingDetailsByIdForUser
    )

router
    .route('/:id')
    .get(bookingController.getBooking)
    .patch(bookingController.updateBooking)
    .delete(bookingController.deleteBooking)

module.exports = router
