
export const hideAlert = () =>{
    const el = document.querySelector('.alert')
    if (el) el.parentElement.removeChild(el)

}
  
  // type can be 'success' or 'error'
export const showAlert = (type, title, message) => {
    console.log(type, title, message); // add this line
    hideAlert()
    const markup = `
      <div class="alert alert--${type}">
        <h3 class="alert__title">${title}</h3>
        <p class="alert__message">${message}</p>
      </div>
    `
    document.querySelector('body').insertAdjacentHTML('afterbegin',markup)
    window.setTimeout(hideAlert, 5000)
}
  
  