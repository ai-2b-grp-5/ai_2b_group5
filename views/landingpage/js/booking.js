import { showAlert } from './alert.js'

export const bookingRegister = async (data) => {
    try {
        const res = await axios({
            method: 'POST',
            url: "http://localhost:4001/api/v1/bookings",
            data
        });

        if (res.data.status === 'success') {
            showAlert('success', 'Success', 'Data Update successfully');
            window.location.reload();
        }

    } catch (err) {
        let message =
            typeof err.response !== 'undefined'
                ? err.response.data.message
                : err.message;
        showAlert('error', message);
    }
};

const userDataForm = document.querySelector('.booking-car');

userDataForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const carModel = document.getElementById('carModel').value;
    const carColor = document.getElementById('vehicleColour').value;
    const carNumber = document.getElementById('carPlateNumber').value;
    const phoneNumber = document.getElementById('phoneNumber').value;
    const startingTime = document.getElementById('startingTime').value;
    const startingDate = document.getElementById('startingDate').value;
    
    const data = {
        carModel,
        carColor,
        carNumber,
        phoneNumber,
        startingTime,
        startingDate
    };
    
    bookingRegister(data);
});





// import { showAlert } from './alert.js';

// // A function to update user settings data
// const updateUserSettings = async (userData) => {
//   try {
//     const res = await axios({
//       method: 'PATCH',
//       url: 'http://localhost:4001/api/v1/user-settings',
//       data: userData,
//     });

//     if (res.data.status === 'success') {
//       console.log('User settings updated successfully');
//     }
//   } catch (err) {
//     console.error(err);
//   }
// };

// // A function to register a new booking
// export const bookingRegister = async (data) => {
//   try {
//     const res = await axios({
//       method: 'POST',
//       url: 'http://localhost:4001/api/v1/bookings',
//       data,
//     });

//     if (res.data.status === 'success') {
//       // Update user settings with the car model and color
//       const userData = { carModel: data.carModel, carColor: data.carColor };
//       await updateUserSettings(userData);

//       showAlert('success', 'Success', 'Booking registered successfully');
//       window.location.reload();
//     }
//   } catch (err) {
//     let message =
//       typeof err.response !== 'undefined'
//         ? err.response.data.message
//         : err.message;
//     showAlert('error', message);
//   }
// };

// const userDataForm = document.querySelector('.booking-car');

// userDataForm.addEventListener('submit', (e) => {
//   e.preventDefault();
//   const carModel = document.getElementById('carModel').value;
//   const carColor = document.getElementById('vehicleColour').value;
//   const carNumber = document.getElementById('carPlateNumber').value;
//   const phoneNumber = document.getElementById('phoneNumber').value;
//   const startingTime = document.getElementById('startingTime').value;
//   const startingDate = document.getElementById('startingDate').value;

//   const data = {
//     carModel,
//     carColor,
//     carNumber,
//     phoneNumber,
//     startingTime,
//     startingDate,
//   };

//   bookingRegister(data);
// });

