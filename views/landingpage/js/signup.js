

// import { showAlert } from "./alert.js";

// export const signup = async (name, email, password, passwordConfirm) => {
//   try {
//     const res = await axios({
//       method: "POST",
//       url: "http://localhost:4001/api/v1/users/signup",
//       data: {
//         name,
//         email,
//         password,
//         passwordConfirm,
//       },
//     });
//     if (res.data.status === "success") {
//       window.setTimeout(() => {
//         console.log('Timeout callback called');
//         location.assign("/");
//         showAlert("success", "Account created successfully");
//       }, 5000);
//     }
//   } catch (err) {
//     let message =
//       typeof err.response !== "undefined"
//         ? err.response.data.message
//         : "An error occurred. Please try again later.";
    
//     if (message.includes("Password")) {
//         showAlert("error", "Error", "Passwords do not match");
//     } else {
//       showAlert("error", "Error", message);
//     }
//   }
// };

// document.querySelector(".form").addEventListener("submit", (e) => {
//   e.preventDefault();
//   const name = document.getElementById("name").value;
//   const email = document.getElementById("email").value;
//   const password = document.getElementById("password").value;
//   const passwordConfirm = document.getElementById("password-confirm").value;

//   signup(name, email, password, passwordConfirm);
// });



import { showAlert } from "./alert.js";

export const signup = async (name, email, password, passwordConfirm) => {
  try {
    const res = await axios({
      method: "POST",
      url: "http://localhost:4001/api/v1/users/signup",
      data: {
        name,
        email,
        password,
        passwordConfirm,
      },
    });
    if (res.data.status === "success") {
      window.setTimeout(() => {
        console.log('Timeout callback called');
        location.assign("/");
        showAlert("success", "Sucess","Account created successfully");
      }, 5000);
    }
  }catch (err){
    let message = "An error occurred. Your password donot match.";
  if (err.response && err.response.data && err.response.data.message) {
    message = err.response.data.message;
  }
  if (message.includes("Password")) {
    showAlert("error", "Error", "Passwords do not match");
  } else {
    showAlert("error", "Error", message);
  }
  }
};

document.querySelector(".form").addEventListener("submit", (e) => {
  e.preventDefault();
  const name = document.getElementById("name").value;
  const email = document.getElementById("email").value;
  const password = document.getElementById("password").value;
  const passwordConfirm = document.getElementById("password-confirm").value;

  signup(name, email, password, passwordConfirm);
});


